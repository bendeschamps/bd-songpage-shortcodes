bd-songpage-shortcodes
======================

A plugin to simplify making nice shop pages in WooCommerce

Version 0.6. Refactored and ready for testing!

New features:
- Merch shortcode, with Product Description
(Advanced->Attributes->'product')
- removed images for BUY ALL MP3S and BUY CD, replaced with HTML
- fixed CSS
- refactored HTML & PHP output.
- added more navigation options
- corrected "Plugin URI" field - about bloody time too!
