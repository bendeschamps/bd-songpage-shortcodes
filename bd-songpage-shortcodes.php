<?php
/*
Plugin Name: Ben's Song-Page Shortcodes
Plugin URI: https://gitlab.com/t8pe/bd-songpage-shortcodes
Description: Plugin to enable Album & Song shortcodes in WordPress without Woo stupidity
Version: 0.6
Author: Ben Deschamps
Author URI: http://bendeschamps.com
License: GPLv2
*/

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}


function bd_songpage_scripts() {
	wp_enqueue_style( 'bd-songpage-css', plugins_url( 'bd-songpage-shortcodes/css/style.css' ) );
}

add_action( 'wp_enqueue_scripts', 'bd_songpage_scripts' );

// This sets up the icons in a convenient place!

define ( 'CART_ICON', 'ic_shopping_cart_black_25dp.png' );
define ( 'INFO_ICON', 'ic_info_outline_black_25dp.png' );
define ( 'BUY_CD_ICON', 'BUTTON-BuyCD-80x27.png');
define ( 'BUY_MP3_ICON', 'BUTTON-BuyAllMP3-129x27.png');
define ( 'ICON_PATH', plugins_url( 'images/', __FILE__ ));

// ALBUM SHORTCODE //
function bdAlbumShortcode( $slug, $enclosure = null ) {

  $albumslug = $slug[0]; // because $slug is an array not an object
  $albuminfo = get_term_by( 'slug', $albumslug, 'product_cat' ); 
 
// These extract the IDs of the products in question & construct buy-now links for them.
  $getCD = get_page_by_title( $albuminfo->name . " (physical CD)", OBJECT, 'product' );
  $getMP3 = get_page_by_title( $albuminfo->name . " (Full Album Download)", OBJECT, 'product' );

  $CDproduct = new WC_product( $getCD->ID );
  $MP3product = new WC_product( $getMP3->ID );
  
// This sets up the thumbnail. I can change the size of it as desired.
  $thumbnail_id = get_woocommerce_term_meta( $albuminfo->term_id, 'thumbnail_id', true );
  $image = wp_get_attachment_url( $thumbnail_id );
  $album_img = '<img src="' . $image . '" width="140px" height="140px" />';

return
       '<div class="hd-album-container">
         <div class="hd-album">
          <div class="hd-album-thumbnail">
	  <img src="'
       . $image
       . '" width="140px" height="140px" />
	</div> <!-- hd-album-thumbnail -->
	<div class="hd-album-title"><h3 id="'
       . $albuminfo->name
  . '">'
  . $albuminfo->name
       . '</h3></div> <!-- hd-album-title -->
	<ul class="albumlist">
	 <li class="album">
	 <div class="hd-album-individual-media">
	  <em>Buy All MP3s</em>
	   
	 <div class="hd-album-buynow">
	  <a href="'
       . get_site_url() . '?add-to-cart=' . $getMP3->ID . '"> <img src="' . ICON_PATH . CART_ICON . '" /></a>  $'
       . $MP3product->regular_price
       . ' </div> <!-- hd-album-buynow -->
	  <div class="hd-album-moreinfo">
	   <a href="'
       . get_site_url() . '/album_wiki/#' . $albuminfo->slug . '"><img src="' . ICON_PATH . INFO_ICON
       . '" /></a>
	  </div> <!-- hd-album-moreinfo -->
	 </li>
	 <li class="album">
	 <div class="hd-album-individual-media">
	 <em> Buy Physical CD</em>
	 <div class="hd-album-buynow">
	   <a href="'
       . get_site_url() . '?add-to-cart=' . $getCD->ID . '"> <img src="' . ICON_PATH . CART_ICON . '" /></a>  $'
       . $CDproduct->regular_price
       . '</div><!-- hd-album-buynow -->
	  <div class="hd-album-moreinfo">
	   <a href="'
       . get_site_url() . '/album_wiki/#' . $albuminfo->slug . '"><img src="' . ICON_PATH . INFO_ICON
       . '" /></a>
	  </div><!-- hd-album-moreinfo -->
	 </li>
	</ul>  
      </div><!-- hd-album --><ul class="songlist">'
	. do_shortcode( $enclosure )
	. '</ul> <!-- to tie into the Songs shortcode -->
<a href="#TOP">Back to Top</a>
    </div> <!-- hd-album-container -->';
}
add_shortcode( 'Album', 'bdAlbumShortcode' );

// MERCH SHORTCODE //
function bdMerchShortcode( $merchID ) {
  $merch = $merchID[0];
  $product = new WC_product( $merch ); 

  $image_link  	= wp_get_attachment_url( get_post_thumbnail_id( $merch ) );

  $merch_img = '<img src="' . $image_link . '" width="140px" height="140px" />';

  // Check to see if the Merch is physical or virtual - to determine whether to show merch_wiki or album_wiki //
  if ((get_post_meta( $merch, '_virtual', true)) == 'yes') {
      $info_line = '<a href="' . get_site_url() . '/album_wiki/#' . $product->post->post_name . '"><img src="' . ICON_PATH . INFO_ICON . '" /></a>';
  	} else {
      $info_line = '<a href="' . get_site_url() . '/merch_wiki/#' . $product->post->post_name . '"><img src="' . ICON_PATH . INFO_ICON . '" /></a>';
  	}
     
return

 '<div class="hd-individual-merch"> 
  <div class="hd-merch-thumbnail">
  <img src="' . $image_link . '" width="140px" height="140px" />
  </div> <!-- hd-merch-thumbnail -->
  <div class="hd-merch-title"><h3 id="'
  . get_the_title( $merch )
   . '">'
  . get_the_title( $merch )
  . '</h3></div> <!-- hd-merch-title -->
  <div class="hd-merch-price"> $'
  . $product->regular_price
  . '</div> <!-- hd-merch-price -->
  <div class="hd-merch-buynow">
  <a href="' . get_site_url() . '?add-to-cart=' . $product->id . '"><img src="' . ICON_PATH . CART_ICON . '" /></a>
  </div> <!-- hd-merch-buynow -->
  <div class="hd-merch-moreinfo">'
    . $info_line 
 . '</div> <!-- hd-merch-moreinfo -->
</div> <!-- hd-individual-merch -->
<p>'; 
} 
add_shortcode( 'Merch', 'bdMerchShortcode' );

function bdSongShortcode( $songID ) {

  // Again, extracting the ID from the array returned by $songID
  $song = $songID[0];
  $product = new WC_product( $song ); 
  $mp3_file = get_post_meta( $song, "mp3", true );
// Determine if there is a playable mp3 sample & cue it, otherwise put the song title:
  if ( $mp3_file ) {
    $mp3_or_title = do_shortcode( '[mp3t track="' . get_post_meta( $song, "mp3", true ) . '" title="' . get_the_title( $song ) . '" play="" stop="" ind=""  ]' );
  } // change this to mp3_file
  else {
    $mp3_or_title = get_the_title( $song );
  }

return
  '<li class="song">
<div class="hd-album-individual-song">
  <div class="hd-album-song-mp3j">'
  . $mp3_or_title
  . '</div><!-- hd-album-song-mp3j-->
<div class="hd-album-song-price"> $'
. $product->regular_price
. '</div><!--hd-album-song-price-->
<div class="hd-album-song-buynow">
<a href="'
. get_site_url() . '?add-to-cart=' . $product->id . '"><img src="' . ICON_PATH . CART_ICON . '" /></a>
</div><!--hd-album-song-buynow-->
<div class="hd-album-song-moreinfo">
  <a href="'
. get_site_url() . "/song_wiki/#" . get_the_title( $song ). '">
<img src="' . ICON_PATH . INFO_ICON . '" /></a>
</div><!--hd-album-song-moreinfo-->
</div><!--hd-album-individual-song-->
</li>'; 
}

add_shortcode( 'Song', 'bdSongShortcode' );

?>